import React, {} from 'react';
import { Button, ButtonGroup } from 'react-bootstrap';
import './Common.css';
import './QuantityComponent.css';

class QuantityComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    Update = (action, item) => {
        if (action === "more" && item.qty < item.maxQty) {
            item.qty++;
        }
        else if (action === "less") {
            if (item.qty > 0)
                item.qty--;
        }
        this.props.onQtyUpdated( item );
    }

    View = (item) => {
        var isAtMin = item.qty > 0 ? false : true;
        var isAtMax = item.qty < item.maxQty ? false : true;
        const jsx = (
            <ButtonGroup className="qtySelect flexCentered" aria-label="Qty-Select">
                <Button bsPrefix="qtySelect-button" onClick={this.Update.bind(undefined, "less", item)} disabled={isAtMin}>-</Button>
                <div className="qtySelect-qty">{item.qty === undefined ? 0 : item.qty}</div>
                <Button bsPrefix="qtySelect-button" onClick={this.Update.bind(undefined, "more", item)} disabled={isAtMax}>+</Button>
            </ButtonGroup>
        );
        return jsx;
    }

    render() {
        return (
            this.View(this.props.item)
        );
    }
}

export default QuantityComponent;