import React from 'react';
import Button from 'react-bootstrap/Button';
import { Container, Row, ButtonGroup } from 'react-bootstrap';
import './Rts.css';

class Rts extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date(),
                      reservedTs : null};
    }
    
    removeSlot = () => {
        this.props.onTsReserved(null, null);
        this.setState({ reservedTs: null });
        clearTimeout(this.timeSlotTimeOut);
    }

    onTsReserved = (ts) => {
        this.timeSlotTimeOut = setTimeout(() => this.removeSlot(), 900000);
        this.props.onTsReserved(ts, this.state.date.getTime());
        this.setState({ reservedTs: this.state.date.getTime()});
    }

    isExceeded = (h, m) => {
        var currentH = this.state.date.getHours();
        var currentM = this.state.date.getMinutes();

        if ((currentH > h) || (currentH === h && currentM > m))
            return true;
        return false;
    }

    build = () => {
        const   elemC = 4;
        const   elemR = 4;
        var     rtsRender = [];
        var     serviceStartAt = 11;
        var     h = this.state.date.getHours();
        var     timeSlots = [];

        if (h <= 15)
            serviceStartAt = 11;
        else if (h > 15 && h < 24)
            serviceStartAt = 19;
        for (let i = 0; i < elemR; i++) {
            for (let c = 0; c < elemC; c++) {
                timeSlots[c] = `${i + serviceStartAt}:${c === 0 ? '00' : c * 15} - ${(c === 3 ? i + 1 : i) + serviceStartAt}:${c === 3 ? '00' : c * 15 + 15}`;
            }
            rtsRender.push(
            <Row key={i}>
                    <ButtonGroup size="lg" className="btn-group-1">
                        <Button onClick={this.onTsReserved.bind(undefined, timeSlots[0])} disabled={this.isExceeded(i + serviceStartAt, 15)} variant="secondary">{timeSlots[0]}</Button>{' '}
                        <Button className="btn-1-border" onClick={this.onTsReserved.bind(undefined, timeSlots[1])} disabled={this.isExceeded(i + serviceStartAt, 30)} variant="secondary">{timeSlots[1]}</Button>{' '}
                        <Button className="btn-1-border" onClick={this.onTsReserved.bind(undefined, timeSlots[2])} disabled={this.isExceeded(i + serviceStartAt, 45)} variant="secondary">{timeSlots[2]}</Button>{' '}
                        <Button className="btn-1-border-left" onClick={this.onTsReserved.bind(undefined, timeSlots[3])} disabled={this.isExceeded(i + serviceStartAt, 59)} variant="secondary">{timeSlots[3]}</Button>{' '}
                </ButtonGroup>
            </Row>);
        }
        return rtsRender;
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    componentDidMount () {
        this.timerID = setInterval(
            () => this.tick(),
            () => this.build(),
            1000
        );
    }

    tick() {
        this.setState({
            date: new Date(),
        });
    }
    render() {
        return (
            <div id="rts" className="rts">
                <h2>Please choose the desired timeslot</h2>
                    {this.build()}
            </div>
        );
    }
}
export default Rts;