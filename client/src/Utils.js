export function CartIter(cart, funct){
    for (let e of cart){
        funct(e);
    }
};
