import React from 'react';
import './Clock.css';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }
    
    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }
    componentWillUnmount() {
        clearInterval(this.timerID);
    }
    tick() {
        this.setState({
            date: new Date()
        });

        this.props.onDateUpdate(this.state.date);
    }
    render() {
        return (
        <div className="clock">
            <h6>{this.state.date.toLocaleString()}</h6>
        </div>
        );
    }
}
export default Clock;