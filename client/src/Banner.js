import React from 'react';
import { Nav, Row, Col } from 'react-bootstrap';
import './Banner.css';
import './Common.css';

class Banner extends React.Component {
    constructor(props) {
        super(props);
    }

    newItemModule = () => {
        return this.props.newItemCounter != 0 ? this.props.newItemCounter : " ";
    }

    render() {
        const newItemCounter = this.newItemModule();

        return (
            <div className="banner">
                <p className="bannerTitle">The name of your restaurant</p>
                {this.props.timeSlot.reservedTs &&
                <div className="pickupTsContainer">
                    <p>Pickup TimeSlot:</p>
                    <p className="pickupTsReserverTs">{this.props.timeSlot.reservedTs}</p>
                </div>
                }
                <Nav className="shoppingCartContainer">
                    <Nav.Link className="flexCentered" href="#cart">
                        <i className="material-icons-outlined md-light shoppingCartIcon">
                            shopping_cart
                        </i>
                        <span className="shoppingCartQty">{newItemCounter}</span>
                    </Nav.Link>
                </Nav>

            </div>);
    }
}

class BannerSmd extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        console.log("bannerSmd")
        return (
            <>
            </>
        );
    }
}

export  {Banner, BannerSmd};