import React from 'react';
import './IsOpen.css';
class IsOpen extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className="IsOpen">
                <h6>We are open</h6>
            </div>
        );
    }
}
export default IsOpen;
