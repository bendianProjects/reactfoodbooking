import React, { useState } from 'react';
import { Dropdown, Alert, Image, Card, Badge, Button, ToggleButton, ListGroup, Row, Col, Container, ButtonGroup } from 'react-bootstrap';
import './Menu.css';
import QuantityComponent from './QuantityComponent.js';
import foodImg from './img/assietteviande.jpg';

class Menu extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            Options: { itemKey: null },
            menuItems: {},
            alertEmptyQuantity: null,
            isArticleAddedToCart: null
        }
    }

    componentDidMount() {
        const fakeMenuItems = {
            starters: [
                { key: "1", qty: 0, maxQty: 10, aviableQty: 20, imgPath: "../img/assietteviande.jpg", name: "poulet tikka", desc: "blablabla", price: 7, options: [{ key: "1.1", name: "Supp citron", desc: "blablabla", price: 5, qty: 0, maxQty: 2 }, { key: "1.2", name: "Supp boulghour", desc: "blablabla", price: 3, qty: 0, maxQty: 2 }] },
                { key: "2", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 7, options: [{ key: "4.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "4.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
            ],
            mainCourse: [
                { key: "5", qty: 0, maxQty: 10, aviableQty: 20, name: "taboule libanais", desc: "blablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla", price: 15, options: [{ key: "5.1", name: "Supp citron", desc: "blablabla", price: 5, qty: 0, maxQty: 2 }, { key: "5.2", name: "Supp boulghour", desc: "blablabla", price: 3, qty: 0, maxQty: 2 }] },
                { key: "6", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "6.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "6.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
                { key: "7", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "7.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "7.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
                { key: "8", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "8.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "8.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
            ],
            desert: [
                { key: "9", qty: 0, maxQty: 10, aviableQty: 20, name: "Tiramisu", desc: "blablabla", price: 8 },
                { key: "10", qty: 0, maxQty: 10, aviableQty: 20, name: "Creme brulee", desc: "blablabla", price: 15 },
                { key: "11", qty: 0, maxQty: 10, aviableQty: 20, name: "profiteroles", desc: "blablabla", price: 10 }
            ],
            boissons: [
                { key: "12", qty: 0, maxQty: 10, aviableQty: 20, name: "Coca 35cl", desc: "blablabla", price: 2 },
                { key: "13", qty: 0, maxQty: 10, aviableQty: 20, name: "Fanta 35cl", desc: "blablabla", price: 2 },
                { key: "14", qty: 0, maxQty: 10, aviableQty: 20, name: "Desperados 50cl", desc: "blablabla", price: 7 }
            ]
        };
        this.setState({ menuItems: fakeMenuItems })
    }

    elementAdded = (item) => {
        const removeAlert = () => {
            this.setState({ isArticleAddedToCart: null });
            clearTimeout(this.elementAddedTimeout);
        }
        if (this.elementAddedTimeout)
            clearTimeout(this.elementAddedTimeout);
        this.setState({ isArticleAddedToCart: item.key });
        this.elementAddedTimeout = setTimeout(() => removeAlert(), 5000);


        //reset quantity of menuItems
        item.qty = 0;
        if (item.options) {
            item.options.forEach(opt => {
                opt.qty = 0;
            });
        }
    }

    AddToCart = (item, type) => {
        //if quantity not selected
        const timeout = () => {
            this.setState({ alertEmptyQuantity: null });
            clearTimeout(this.alertTimeout);
        }
        if (item.qty < 1) {
            if (this.alertTimeout)
                clearTimeout(this.alertTimeout);
            this.setState({ alertEmptyQuantity: item.key });
            this.alertTimeout = setTimeout(() => timeout(), 5000);
            return;
        }

        this.props.onCartAdd(item, type, this.elementAdded);
    }

    ItemQtyUpdate = (item) => {
        this.setState({ menuItems: this.state.menuItems });
    }

    onOptionDropClicked = (item) => {
        var tmpOpt = { itemKey: item.key };
        this.setState({ Options: tmpOpt });
    }

    onShowOptionDropdown = (item) => {
        if (item.key === this.state.Options.itemKey)
            return true;
        return undefined;
    }

    onOptDropdownSelected = (item, visible, event, d) => {
        if (item.key === this.state.Options.itemKey && visible === false && event === false) {
            var tmpOpt = { itemKey: null };
            this.setState({ Options: tmpOpt });
        }
    }

    OptModule = (item) => {
        var optMap = [];
        if (item.hasOwnProperty("options")) {
            optMap = item.options.map((opt, key) => {
                var jsx =
                    <Dropdown.Item key={key} onClick={this.onOptionDropClicked.bind(undefined, item)}>
                        <div className="item item-options flexRow">
                            <p>{opt.name} ({opt.price} $)</p>
                            <QuantityComponent item={opt} onQtyUpdated={this.ItemQtyUpdate} />
                        </div>
                    </Dropdown.Item>
                return jsx;
            });
            return (

                <Dropdown onToggle={this.onOptDropdownSelected.bind(undefined, item)} show={this.onShowOptionDropdown(item)}>
                    <Dropdown.Toggle variant="outline-success" id="dropdown-basic">
                        Options
            </Dropdown.Toggle>

                    <Dropdown.Menu >
                        {optMap}
                    </Dropdown.Menu>
                </Dropdown>
            );
        }
        return null;
    }

    FinalPriceModule = (item) => {
        var total = item.price * item.qty;
        if (item.options) {
            for (let opt of item.options) {
                if (total > 0)
                    total += (opt.price * opt.qty);
            }
        }
        const jsx = (
            <div className={total != 0 ? "bold" : ""}>{total}</div>
        );
        return jsx;
    }

    CreateMenu = (items) => {
        var menuRender = [];
        var tmpContainer = [];
        for (let e in items) {
            var listItems = items[e].map(item => {
                const optModule = this.OptModule(item);
                const finalPriceModule = this.FinalPriceModule(item);

                var itemContent =
                    <>
                        <Row>
                            <Col xs={{ span: 3 }} sm={{ span: 3 }} md={{ span: 3 }} className="item item-name flexCentered"><p>{item.name}</p></Col>
                            <Col xs={{ span: 2 }} sm={{ span: 3 }} md={{ span: 3 }} className="item item-basePrice flexCentered">{item.price} $ </Col>
                            <Col xs={{ span: 4 }} sm={{ span: 3 }} md={{ span: 3 }} className="item item-qty flexCentered"><QuantityComponent item={item} onQtyUpdated={this.ItemQtyUpdate} /></Col>
                            <Col xs={{ span: 3 }} sm={{ span: 3 }} md={{ span: 3 }} className="item item-finalPrice flexCentered">Total {' '}{finalPriceModule}{' '} $</Col>
                        </Row>
                        <Row>
                            <Col xs={{ span: 3 }} sm={{ span: 3 }} md={{ span: 3 }} className="item flexCentered"><img className="item-img" src={foodImg} width="120" height="80" /></Col>
                            <Col xs={{ span: 4 }} sm={{ span: 5 }} md={{ span: 5 }} className="item item-desc flexCentered">{item.desc}</Col>
                            <Col xs={{ span: 2 }} sm={{ span: 2 }} md={{ span: 2 }} className="item item-optionBtn flexCentered">{optModule &&
                                <ListGroup >
                                    {optModule}
                                </ListGroup>
                            }</Col>
                            <Col xs={{ span: 2, offset: 1 }} sm={{ span: 2, offset: 0 }} md={{ span: 2, offset: 0 }} className="item item-addToCart flexCentered">
                                <Button variant="outline-success" onClick={this.AddToCart.bind(undefined, item, e)}>Add to cart</Button>{' '}
                            </Col>
                        </Row>
                        <Alert show={item.key === this.state.alertEmptyQuantity ? true : false} key={`alertEmptyQuantity ${item.key}`} variant="warning">
                            Please choose a quantity before add to cart
                        </Alert>
                        <Alert show={item.key === this.state.isArticleAddedToCart ? true : false} key={`isArticleAddedToCart ${item.key}`} variant="success">
                            Item succesfully added to cart
                        </Alert>
                    </>
                var retList =
                    <li key={item.key} id={item.key} className="listMenu">
                        <Container className="itemContent">
                            {itemContent}
                        </Container>
                    </li>
                return retList;
            });
            tmpContainer.push(
                <Col key={tmpContainer.length} className="menuRenderCol">
                    <div key={menuRender.length} className="menu">
                        <h4>{e}</h4>
                        <ul>
                            {listItems}
                        </ul>
                    </div>
                </Col>
            );
            if (tmpContainer.length === 2) {
                menuRender.push(<Row key={menuRender.length} xs={1} md={2}>{tmpContainer}</Row>)
                tmpContainer = [];
            }
        }
        tmpContainer = [];
        return menuRender;
    }

    render() {
        return (
            this.CreateMenu(this.state.menuItems)
        );
    }
}
export default Menu;
