import React from 'react';
import { Table, } from 'react-bootstrap';
import QuantityComponent from './QuantityComponent.js';
import './Cart.css';

class Cart extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

    }

    ItemQtyUpdate = (item) => {
        this.props.onModCartItem(item);
    }

    optModule = (options) => {
        if (options === undefined)
            return null;
        const optRender = options.map((opt, key) => {
            const ret =
                <p key={key}>{opt.name} <span>( {opt.qty} )</span></p>
            return ret;
        });
        return <div className="cartOptions">{optRender}</div>;
    }

    render() {
        var cartTd = [];
        var cartRender;
        var totalAmmount = 0;
        var empty = true;

        this.props.cart.forEach(element => {
            if (element.items.length)
                empty = false;
            cartTd.push(element.items.map((item, key) => {
                totalAmmount += item.price * item.qty;
                const listItems = (
                    <>
                        {key === 0 && <tr key={`title${key}`}><td className="elementType">{element.type}:</td><td></td><td></td><td></td></tr>}
                        <tr key={key} className="elementContent">
                            <td>{item.name}</td>
                            <td> <QuantityComponent item={item} onQtyUpdated={this.ItemQtyUpdate} /></td>
                            <td>{item.price * item.qty} $</td>
                            <td>{this.optModule(item.options)}</td>
                        </tr>
                    </>);
                return listItems;
            }));
        });

        if (empty)
            cartRender = <div>Your cart is empty</div>;
        else
            cartRender = (
                <>
                    <Table borderless responsive="sm">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            {cartTd}
                        </tbody>
                    </Table>
                </>);
        return (
            <div className="cart" id="cart">
                <h2>Cart</h2>
                {/* <div className="cart"></div> */}
                <div className="listCartContainer">{cartRender}</div>
                <p className="cartTotal">Total TTC: <span>{totalAmmount} $</span></p>
            </div>
        );
    }
}

export default Cart;
