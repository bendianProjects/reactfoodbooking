import React from 'react';
import Rts from './Rts';
import Menu from './Menu';
import Cart from './Cart';
import Checkout from './Checkout';
import RightPanel from './RightPanel';
import {Banner, BannerSmd} from './Banner';
import { Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cart: [],
      newItemCounter: 0,
      timeSlot: { reservedTs: null }
    }
  }

  componentDidMount() {
    //faire un HOC car partage de la methode avec menu
    const cart = [];
    const fakeMenuItems = {
      starters: [
        { key: "1", qty: 0, maxQty: 10, aviableQty: 20, imgPath: "../img/assietteviande.jpg", name: "poulet tikka", desc: "blablabla", price: 7, options: [{ key: "1.1", name: "Supp citron", desc: "blablabla", price: 5, qty: 0, maxQty: 2 }, { key: "1.2", name: "Supp boulghour", desc: "blablabla", price: 3, qty: 0, maxQty: 2 }] },
        { key: "2", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 7, options: [{ key: "4.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "4.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
      ],
      mainCourse: [
        { key: "5", qty: 0, maxQty: 10, aviableQty: 20, name: "taboule libanais", desc: "blablablablablablablablablablablablablablablablablablablablablablablablablablablablablabla", price: 15, options: [{ key: "5.1", name: "Supp citron", desc: "blablabla", price: 5, qty: 0, maxQty: 2 }, { key: "5.2", name: "Supp boulghour", desc: "blablabla", price: 3, qty: 0, maxQty: 2 }] },
        { key: "6", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "6.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "6.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
        { key: "7", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "7.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "7.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
        { key: "8", qty: 0, maxQty: 10, aviableQty: 20, name: "Beignet oignons", desc: "blablabla", price: 15, options: [{ key: "8.1", name: "Sauce roquefort", desc: "blablabla", price: 0, qty: 0, maxQty: 2 }, { key: "8.2", name: "Supp boulghour", desc: "blablabla", price: 2, qty: 0, maxQty: 2 }] },
      ],
      desert: [
        { key: "9", qty: 0, maxQty: 10, aviableQty: 20, name: "Tiramisu", desc: "blablabla", price: 8 },
        { key: "10", qty: 0, maxQty: 10, aviableQty: 20, name: "Creme brulee", desc: "blablabla", price: 15 },
        { key: "11", qty: 0, maxQty: 10, aviableQty: 20, name: "profiteroles", desc: "blablabla", price: 10 }
      ],
      boissons: [
        { key: "12", qty: 0, maxQty: 10, aviableQty: 20, name: "Coca 35cl", desc: "blablabla", price: 2 },
        { key: "13", qty: 0, maxQty: 10, aviableQty: 20, name: "Fanta 35cl", desc: "blablabla", price: 2 },
        { key: "14", qty: 0, maxQty: 10, aviableQty: 20, name: "Desperados 50cl", desc: "blablabla", price: 7 }
      ]
    };
    for (let e in fakeMenuItems) {
      cart.push({ type: e, items: [] });
    }
    this.setState({ cart: cart })
  }
  //rendre cette fonction generique en utilisant obj.keys => si modification de la structure de Menuitems pour ne pas avoir a se la retaper ici
  newCartItem = (item) => {
    var newItem = {
      key: item.key,
      name: item.name,
      price: item.price,
      qty: item.qty,
      maxQty: item.maxQty
    };

    if (item.options) {
      newItem.options = [];
      item.options.forEach(elem => {
        newItem.options.push({ key: elem.key, name: elem.name, price: elem.price, qty: elem.qty, maxQty: elem.maxQty })
      });
    }
    return newItem;
  }

  handleModCartItem = (item) => {
    var tmpCart = this.state.cart;
    let tmpNewItemCounter = 0;

    for (let e of tmpCart) {
      if (e.items.length) {
        for (let i = 0; i < e.items.length; i++) {
          tmpNewItemCounter += e.items[i].qty;
          if (e.items[i].key === item.key && item.qty < 1) {
              e.items.splice(i, 1);
          }
        }
      }
    }
    this.setState({
      cart: tmpCart,
      newItemCounter: tmpNewItemCounter
    })
  }

  cmpOpt = (optA, optB) => {
    for (var i = 0; i < optA.length; i++) {
      if (optA[i].qty != optB[i].qty)
        return 1;
    }
    return 0;
  }

  addElementToCart = (item, cartType) => {
    var cartItems = cartType.items;
    for (var i = 0; i < cartItems.length; i++) {
      if (((cartItems[i].key === item.key) &&
        (item.options && !this.cmpOpt(cartItems[i].options, item.options)))
        ||
        (!item.options && cartItems[i].key === item.key)) {
        cartItems[i].qty += item.qty;
        return;
      }
    }
    cartItems.push(this.newCartItem(item))
  }

  handleCartAdd = (item, type, callback) => {
    let newCart = this.state.cart;
    for (let i = 0; i < newCart.length; i++) {
      if (newCart[i].type === type) {
        this.addElementToCart(item, newCart[i]);
      }

    }
    let qty = item.qty;
    this.setState((state) => ({
      newItemCounter: state.newItemCounter + qty,
      cart: newCart
    }));
    callback(item);
  }

  handleTsReserved = (ts, timeReservedTs) => {
    let tmpTimeSlot = { reservedTs: ts, timeReservedTs: timeReservedTs };
    this.setState({ timeSlot: tmpTimeSlot });
  }

  render() {

    return (
      <>
        <Banner newItemCounter={this.state.newItemCounter} timeSlot={this.state.timeSlot} />
        <RightPanel timeSlot={this.state.timeSlot} />
        <Container fluid>
          <Menu onCartAdd={this.handleCartAdd} />
          <Rts onTsReserved={this.handleTsReserved} />
          <Cart cart={this.state.cart} onModCartItem={this.handleModCartItem} />
          <Checkout timeSlot={this.state.timeSlot} cart={this.state.cart}/>
        </Container>
      </>
    );
  }
}

export default App;
