import React from 'react';
import { Nav } from 'react-bootstrap';
import IsOpen from './IsOpen';
import Clock from './Clock.js';
import './RightPanel.css';


class RightPanel extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: null }
    }

    handleDateUpdate = (date) => {
        this.setState({ date: date });
    }

    render() {
        var reservedTs = null;
        var aviableCart = new Date;

        if (this.props.timeSlot.reservedTs !== null) {
            reservedTs = this.props.timeSlot.reservedTs;
        }

        if (this.state.date && this.props.timeSlot.timeReservedTs)
            aviableCart.setTime(this.props.timeSlot.timeReservedTs - this.state.date.getTime() + 900000);

        return (
            <div className="rightPanel">
                <Nav className="flex-column">
                    <Nav.Link href="#root">Menu</Nav.Link>
                    <Nav.Link href="#rts">TimeSlots</Nav.Link>
                    <Nav.Link href="#checkout">Checkout</Nav.Link>
                </Nav>
                <IsOpen />
                {this.props.timeSlot.reservedTs &&
                    <p>Your cart is aviable for {aviableCart.getMinutes()} : {aviableCart.getSeconds()}</p>}
                <Clock onDateUpdate={this.handleDateUpdate} />
            </div>
        )
    }
}

export default RightPanel;
