import App  from './App.js';
 var app = new App;
test('should return a deep copy of item', () => {
  var item = {key: "key1", name: "name1", price: 4, qty: 55, maxQty: 10};
  expect(app.newCartItem(item)).toEqual(item);
});

//test add element to cart 
//test 1
// first send an item
// second send the same item
// verify : -a there is the same number of item -b the item qty have been updated

//test2
// first send a new item different from the first
// verify : -a all the precedent items havent been qty incremented - b new item have been created

//test 3 
// verify than the minimum of attributes is well required to add en element to cart


//test of handleCartAdd ///
//test newitemCounter after have added element
//cart should have item in