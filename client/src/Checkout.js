import React from 'react';
import { Button, Modal } from 'react-bootstrap';
import { CartIter } from './Utils';
import './Checkout.css';
import './Common.css';
import IsOpen from './IsOpen';
import canceled from './img/canceled.svg';

class Checkout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validateClicked: false,
            reservedTs: false,
            emptyCart: true
        }
    }

    isFullyValidateCommand = () => {
        if (this.state.reservedTs === true &&
            this.state.emptyCart === false)
            return true;
        return false;
    }

    checkoutModal = () => {
        const closeModal = () => {
            this.setState({ validateClicked: false });
        }

        const titleModal = () => {
                if (this.isFullyValidateCommand())
                    return (<i class="material-icons green md-36">
                        check_circle
                    </i>);
            else
                    return (<i class="material-icons red md-36">
                        report_problem
                    </i>);
        };

        const bodyModal = () => {
            console.log("in titleModal");
            if (this.state.emptyCart === true) {
                return (<>
                    <h5>Error in your Command</h5>
                    <p>Your Cart is empty</p>
                </>);
            }
            if (this.state.reservedTs !== true) {
                return (<>
                    <h5>Error in your Command</h5>
                    <p>Please select a timeslot</p>
                </>);
            }
            if (this.isFullyValidateCommand())
                return (<>Your Command is validated</>);
            else
                return (<>We encounter a problem with your command please try again</>);
        };

        return (
            <Modal show={this.state.validateClicked}
                onHide={closeModal}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered
            >
                <Modal.Header closeButton>
                    <Modal.Title>{titleModal()}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {bodyModal()}</Modal.Body>
                <Modal.Footer>
                    <Button variant={this.isFullyValidateCommand() ? "success" : "danger"} onClick={closeModal}>
                        Close
          </Button>
                </Modal.Footer>
            </Modal>
        );
    }

    validateCommand = () => {
        let emptyCart = true;
        let reservedTs = false;

        const iscartEmpty = (element) => {
            if (element.items.length)
                emptyCart = false;
        }

        if (this.props.timeSlot.reservedTs !== null)
            reservedTs = true;

        CartIter(this.props.cart, iscartEmpty);

        this.setState({
            reservedTs: reservedTs,
            emptyCart: emptyCart,
            validateClicked: true
        });
    }

    render() {
        const checkoutModal = this.checkoutModal();
        return (
            <div id="checkout" className="checkout">
                <h2>Checkout</h2>
                <Button variant="outline-success" onClick={this.validateCommand}>Validate My Command</Button>
                {this.state.validateClicked && <>{checkoutModal}</>}
            </div>

        );
    }
}
export default Checkout;
